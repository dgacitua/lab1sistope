#include <stdio.h>
#include <stdlib.h>

unsigned long tamanoArchivo(FILE* archivo) {
	unsigned long longitud;

	fseek(archivo, 0UL, SEEK_END);
	longitud = ftell(archivo);
	fseek(archivo, 0UL, SEEK_SET);

	return longitud;
}