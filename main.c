#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/wait.h>
#include "src/cut.h"
#include <math.h>

static int verbose_flag;

int main (int argc, char **argv)
{
  int c,i;
  char * flag,*buffer,nombreOrg[80],partes[80];
  unsigned long longitud, cantidad,resto,pesoArchivo, bytes,cut ;
  
  FILE *Origen, *Destino; 
   

  while (1)
    {
      static struct option long_options[] =
        {
          
          {"cut",   required_argument, 0, 'c'},
          {"join",  required_argument,   0, 'j'},
          {0, 0, 0, 0}
        };
      
      int option_index = 0;

      c = getopt_long (argc, argv, "c:j:",long_options, &option_index);
		  if (c == -1)
        break;

      switch (c)
        {
        case 'c':
          flag=optarg;
          break;

        case 'j':
          flag=optarg;
          break;

        case '?':
          
          break;

        default:
          abort ();
        }
    }
	if (optind < argc)
    {
      printf ("non-option ARGV-elements: ");
      while (optind < argc)
        printf ("%s ", argv[optind++]);
      putchar ('\n');
    }


  //-------------------------------------------------------------------------------------------------------


    cut=atoi(flag);
    
  
  if ((Origen = fopen("fss.mp3", "rb")) == NULL)  {
      printf("No existe el fichero origen!\n");
      return 0;
    } 
   
  fseek(Origen, 0, SEEK_END);
  longitud = ftell(Origen);
  fseek(Origen, 0, SEEK_SET);
  
printf("un archivo de %li bytes, se divide en %li partes. Cada archivo pesa %li, y el ultimo queda con %li bytes, por lo tanto se le deben agregar %li",longitud,cut,pesoArchivo,resto,bytes);
buffer = (char *) malloc (longitud);
  
 unsigned long tamano=ceil(longitud/cut);

for(i=0; i <cut; i++) {
      sprintf(partes, "salida.part%d", i);
      
      if ((Destino = fopen(partes, "wb")) == NULL)  {
        printf("No se ha podido crear el fichero destino!\n");
        return 0;
      } 
  
      fseek(Origen, i*tamano, SEEK_SET);
      cantidad = fread( buffer, 1, tamano, Origen); //buffer guarda el pedazo del archivo como string
      
      if(i<cut-1){

        fwrite(buffer, 1, cantidad, Destino);
      }else{
        //char *temp;
        //char *padding="000";
        //sprintf(temp,"%s%*.*s", buffer,bytes,padding);
        fwrite(buffer,1,tamano,Destino);
      }
      
      
      
    }
    
    //supahfuncion(Origen,Destino,cut);
    fclose(Origen);
    fclose(Destino); 
  return 0;
}

