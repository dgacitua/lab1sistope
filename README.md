# Laboratorio 1 SISTOPE 2015-1

Integrantes:
* Daniel Gacitúa 17.710.090-0
* Ian Orellana	 18.749.235-1

Para compilar:
Use "make debug"

Para ejecutar:
Cortar archivo:
./build/splitter <nombreArchivo> --cut <numeroPartes>

->Esto genera archivo <nombreArchivo.aux> en el directorio, el cuál será leido para
ejecutar el join.

Para unir archivo:
./build/splitter --join <nombreArchivo>

->Funciona cuando están todas las partes del archivo, si falta uno, indica que debe usarse
la función xor, si falta más de uno, o el archivo.xor indica que es imposible juntar el archivo.

Directorio git: https://bitbucket.org/GaciX/lab1sistope
o ejecutar en la terminal: git clone git@bitbucket.org:GaciX/lab1sistope.git




