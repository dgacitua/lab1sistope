#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/wait.h>

#include "funciones.h"

#define PARTES 8

int main (int argc, char **argv)
{
	int c,cut,resto,i;
	char filename[512], filepart[512];
	char* tempPart;
	char* flag,*buffer,nombreOrg[80],nombreDest[80],nombreDest1[80],partes[80];;
	unsigned long longitud, cantidad;

	FILE *Origen, *Destino, *temp; 


	while (1)
	{
		static struct option long_options[] =
		{

			{"cut",   required_argument, 0, 'c'},
			{"join",  required_argument,   0, 'j'},
			{0, 0, 0, 0}
		};

		int option_index = 0;

		c = getopt_long (argc, argv, "c:j:",long_options, &option_index);
		if (c == -1)
			break;

		switch (c)
		{
			case 'c':
			flag=optarg;
			break;

			case 'j':
			flag=optarg;
			break;

			case '?':

			break;

			default:
			abort ();
		}
	}
	if (optind < argc)
	{
		printf ("non-option ARGV-elements: ");
		while (optind < argc)
			printf ("%s ", argv[optind++]);
		putchar ('\n');
	}


//-------------------------------------------------------------------------------------------------------


	cut = PARTES;
	strcpy(filename, flag);

	printf("%i cortes\n", cut);

	for (i=0; i<PARTES; i++) {
		sprintf(filepart, "%s.part%d", filename, i);
		if ((Origen = fopen(filepart, "rb")) == NULL)  {
			printf("No existe la parte %d!\n", i);
			return 1;
		}
		fclose(Origen);
	}
	printf("todo ok\n");

	Destino = fopen(filename, "wb");

	if (Destino == NULL) {
		printf("No se puede crear el archivo regenerado");
		return 1;
	}

	

	for (i=0; i<PARTES; i++) {
		sprintf(filepart, "%s.part%d", filename, i);
		printf("Procesando la parte %d! Archivo: %s\n", i+1, filepart);

		temp = fopen(filepart,"rb");
		if (temp == NULL) {
			printf("No se puede abrir la parte %d! Archivo: %s\n", filepart);
			return 1;
		}

		longitud = tamanoArchivo(temp);
		tempPart = (char*)malloc(longitud);

		cantidad = fread(tempPart, 1, longitud, temp);

		fwrite(tempPart, 1, cantidad, Destino);

		fclose(temp);	
	}


	fclose(Destino);
	printf("Operacion Finalizada!\n");
	return 0;
}