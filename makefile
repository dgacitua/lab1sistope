# MAKEFILE
# Lab1 SISTOPE
# Daniel Gacitúa e Ian Orellana

SOURCES = XOReador JOINeador splitter
DSOURCES = XOReador-dbg JOINeador-dbg splitter-dbg
CFLAGS = -std=c99 -Wall -lm
DFLAGS = -std=c99 -Wall -lm -g3

SDIR = ./src
BDIR = ./build

all: release

release: $(SOURCES)

debug: $(DSOURCES)

splitter:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/splitter.c -o $(BDIR)/splitter $(CFLAGS)

XOReador:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/XOReador.c -o $(BDIR)/XOReador $(CFLAGS)

JOINeador:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/JOINeador.c -o $(BDIR)/JOINeador $(CFLAGS)

splitter-dbg:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/splitter.c -o $(BDIR)/splitter $(DFLAGS)

XOReador-dbg:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/XOReador.c -o $(BDIR)/XOReador $(DFLAGS)

JOINeador-dbg:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/JOINeador.c -o $(BDIR)/JOINeador $(DFLAGS)


.PHONY: clean

clean:
	rm -rf $(BDIR)/*