#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>

#include "biblioteca.h"

#define TRUE 1
#define FALSE 0

#define WRITE 1
#define READ 0

#define FILE_MAX 4096

int main (int argc, char **argv) {
	/* LOGICA DE GETOPT */
	int c, cutFlag, joinFlag, partes, index, pCheck;
	char *fileName, *exe;
	char partesStr[10], cwd[FILE_MAX], path[FILE_MAX];

	cutFlag = joinFlag = FALSE;

	while (TRUE) {
		static struct option long_options[] =
		{
			{"cut",   required_argument, 0, 'c'},
			{"join",  required_argument,   0, 'j'},
			{0, 0, 0, 0}
		};

		int option_index = 0;

		c = getopt_long (argc, argv, "c:j:",long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
			case 'c':
				cutFlag = TRUE;
				partes = atoi(optarg);
				break;
			case 'j':
				joinFlag = TRUE;
				fileName = optarg;
				break;
			case '?':
				break;
			default:
				return 1;
		}
	}

	for (index = optind; index < argc; index++) {
		if (index==optind && cutFlag==TRUE) {
			fileName = argv[index];
		}
		else {
			printf ("Opcion ignorada: %s\n", argv[index]);
		}
	}
    
	/* ================== */

	if (cutFlag==TRUE && joinFlag==TRUE) {		// mas de un argumento
		printf("Hay un exceso de argumentos!\n");
		return 1;
	}
	else if (cutFlag==TRUE && joinFlag==FALSE) {	// modo cut
		// ejecutar cut
		printf("Partes: %d\n", partes);
		printf("ArchivoDividible: %s\n", fileName);

  		exe = formatearEjecutable(argv[0], "splitter", "XOReador");	// se obtiene la ruta relativa del ejecutable

   		sprintf(partesStr, "%d", partes);

   		printf("%s\n", exe);

	   	if (getcwd(cwd, sizeof(cwd))==NULL) {	// se obtiene la ruta absoluta
	   		printf("Error al obtener el directorio de trabajo\n");
	   		return 1;
	   	}

		strcpy(path, cwd);		// se concatena la ruta absoluta con la relativa
		strcat(path, exe);
		printf("%s\n", path);

		pCheck = execl(path, path, fileName, partesStr, NULL);		// ejecutamos exec() con la ruta final 
		printf("Estado de exec(): %d\n", pCheck);

		return pCheck;
	}
	else if (cutFlag==FALSE && joinFlag==TRUE) {	// modo join
		// ejecutar join
		printf("ArchivoUnible: %s\n", fileName);
   		
   		exe = formatearEjecutable(argv[0], "splitter", "JOINeador");	// se obtiene la ruta relativa del ejecutable

   		printf("%s\n", exe);

	   	if (getcwd(cwd, sizeof(cwd))==NULL) {
	   		printf("Error al obtener el directorio de trabajo\n");
	   		return 1;
	   	}

		strcpy(path, cwd);		// se concatena la ruta absoluta con la relativa
		strcat(path, exe);
		printf("%s\n", path);

		pCheck = execl(path, path, fileName, NULL);		// ejecutamos exec() con la ruta final 
		printf("Estado de exec(): %d\n", pCheck);

		return pCheck;
	}
	else {			// no hay suficientes argumentos
		printf("Faltan argumentos!\n");
		return 1;
	}
	return 0;
}