#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "biblioteca.h"
#define TRUE 1
#define FALSE 0

#define WRITE 1
#define READ 0

#define BUFFER 256
#define FILE_MAX 4096

int main(int argc, char** argv) {
	int i,partes,flag=0;
	char *nombreArchivo,*filepart,*tempPart;
	unsigned long longitud,cantidad;
	filepart=(char*)malloc(FILE_MAX*sizeof(char*));
	nombreArchivo=argv[1];
	FILE *entrada=fopen(nombreArchivo,"rb"),*Destino,*temp;


//Condiciones de borde
	if (entrada == NULL) {
		printf("\nNo existe el archivo en el directorio\n");
		return 1;
	}else{
		partes=obtieneNumero(nombreArchivo);
		printf("El archivo fue dividido en %i partes\n",partes);

		for (i=0; i<partes; i++) {
		sprintf(filepart, "%s.part%d", nombreArchivo, i);
		if((entrada=fopen(filepart,"rb"))!=NULL){
			printf("Leyendo parte %d\n", i);
		}else {
			printf("No existe la parte %d!\n", i);
			flag++;
			if(flag==2){
				printf("Falta más de un archivo, XOR no podra restaurar archivo original\n");
				return 1;
			}
		}
		if((entrada=fopen(filepart,"rb"))!=NULL){
			fclose(entrada);
		}
	}
		if(flag==1){
			printf("Debe utilizar la función XOR para restaurar el archivo\n");
			return 1;
		}

//Si cumple los requisitos, lee archivo		
		Destino = fopen("salida", "wb");
//_________________________________________________________________________
		//ciclo JOIN
		for (int i=0;i<partes; ++i){
		sprintf(filepart, "%s.part%d", nombreArchivo, i);
		printf("Procesando la parte %d! Archivo: %s\n", i+1, filepart);

		temp = fopen(filepart,"rb");
		
		if(i<(partes-1)){
		//join normal
		longitud = tamanoArchivo(temp); //funcion que entrega largo de archivo
		tempPart = (char*)malloc(longitud);

		cantidad = fread(tempPart, 1, longitud, temp);

		fwrite(tempPart, 1, cantidad, Destino); //Escribe en archivo de salida
		}else{
			//join que elimina 0's agregados al final del último archvio
			longitud = tamanoArchivo(temp);
			longitud=longitud-atoi(obtieneBytes(nombreArchivo));
			tempPart = (char*)malloc(longitud);
			cantidad = fread(tempPart, 1, longitud, temp);
			fwrite(tempPart, 1, cantidad, Destino);


		}

		fclose(temp);


		}


		fclose(Destino);



		}
	

	printf("Soy el JOINeador!\n");
	return 0;
}