#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

size_t tamanoArchivo(FILE* archivo) {
	size_t longitud;

	fseek(archivo, 0UL, SEEK_END);
	longitud = ftell(archivo);
	fseek(archivo, 0UL, SEEK_SET);

	return longitud;
}

char* formatearEjecutable(char *ruta, char *actualExe, char *newExe) {
	int len = strlen(actualExe);

	if (ruta[0] == '.') {
    	memmove(ruta, ruta+1, strlen(ruta));
	}

	ruta[strlen(ruta)-len] = '\0';

	strcat(ruta, newExe);

	//printf("%s\n", ruta);

	return ruta;
}

int identificarProceso (int codigo) {
	if (codigo == -1) {
		printf("Soy padre, el proceso %d y mi pid es %d\n", codigo, getpid());
	}
	else if (codigo > -1) {
		printf("Soy hijo, el proceso %d y mi pid es %d\n", codigo, getpid());
	}
	else {
		printf("Soy un proceso invalido\n");
	}
	
	return TRUE;
}

int obtieneNumero (char *nombreArchivo){ //Obtiene numero de partes en que fue separado del archivo.aux
	int partes;
	char  *linea,*token,*xorPartes,*aux;
	linea= (char*)malloc(4096*sizeof(char));
	aux= (char*)malloc(4096*sizeof(char));
	sprintf(aux, "%s.aux", nombreArchivo);
	FILE *archivo=fopen(aux,"rb");
	fgets(linea,1000,(FILE*)archivo);
	char lista[50]= "\n";        //Con cada salto de linea se corta otra palabra
    token = strtok(linea,lista);
    xorPartes= (char *)malloc(sizeof(char)*50);  
    partes=atoi(strcpy(xorPartes,token));
	return partes;

}

char *obtieneBytes(char *nombreArchivo){//Obtiene numero de bytes que se agregan desde archivo.aux

	char  *linea,*token,*xorPartes,*aux;
	linea= (char*)malloc(4096*sizeof(char));
	aux= (char*)malloc(4096*sizeof(char));
	sprintf(aux, "%s.aux", nombreArchivo);
	FILE *archivo=fopen(aux,"rb");
	fgets(linea,1000,(FILE*)archivo);
	fgets(linea,1000,(FILE*)archivo);
	char lista[50]= "\n";        //Con cada salto de linea se corta otra palabra
	token = strtok(linea,lista);
    xorPartes = (char *)malloc(sizeof(char)*50);
    strcpy(xorPartes,token);

	return xorPartes;	
}

char *hacerXOR(char *parte1, char *parte2, size_t tamano) {
	size_t j;
	char *resultado = (char*)malloc(tamano);

	for (j=0; j<tamano; j++) {
		resultado[j] = parte1[j] ^ parte2[j];
	}

	return resultado;
}
