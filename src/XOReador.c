#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "biblioteca.h"

#define TRUE 1
#define FALSE 0

#define WRITE 1
#define READ 0

#define FILE_MAX 4096		// tamaño maximo de una ruta de archivo en caracteres
#define BYTES_RW 1 			// bytes por lectura de archivo
#define TROZO 65536			// tamaño de un trozo de buffer de archivo en bytes

int main(int argc, char** argv) {
	int partes, trozos, trozoActual;
	char *nombreArchivo, *nombreParte, *buffer, *xorResult, *aux;		// string buffers
	size_t longitud, tamanoParte, tamanoTrozo, resto, relleno, posicion, offset;		// variables para medir el tamaño de archivos y buffers

	int i = -1; 		// identificador de proceso
	int j;				// contador
	int pid, estado;	// pid y estado del proceso

	int **pipesWrite;	// arreglo de pipes

	FILE *archivoOrigen, *archivoDestino, *archivoAux;	// archivos

	partes = atoi(argv[2]);		// se obtiene por argumento la cantidad de partes del archivo
	nombreArchivo = argv[1];	// se obtiene por argumento el nombre del archivo

	printf("Cortando! Partes: %d Archivo: %s\n", partes, nombreArchivo);

	if ((archivoOrigen = fopen(nombreArchivo, "rb")) == NULL)  {	// se busca el archivo de origen (y se manejan sus errores)
		printf("No existe el archivo de origen!\n");
		return 0;
	} 

	longitud = tamanoArchivo(archivoOrigen);		// obtenemos el tamaño del archivo
	tamanoParte = ceil(longitud/(float)partes);		// obtenemos el tamaño de cada parte
	resto = longitud-(tamanoParte*(partes-1));		// obtenemos el tamaño de la ultima parte
	relleno = tamanoParte-resto;					// obtenemos el tamaño del relleno de cada parte

	posicion = 0;		// posicion en bytes del archivo

	if (tamanoParte>TROZO) {		// el Trozo es el tamaño del buffer de lectura de archivo (en bytes)
		tamanoTrozo = TROZO;
		trozos = ceil(tamanoParte/(float)tamanoTrozo);
		offset = tamanoParte%tamanoTrozo;
	}
	else {
		tamanoTrozo = tamanoParte;
		trozos = 1;
		offset = 0;
	}

	if (resto!=0) {			// texto informativo :-)
		printf("El archivo de %zu bytes, se divide en %d partes.\nCada parte pesa %zu bytes, y el ultimo queda con %zu bytes.\nPor lo tanto se le deben agregar %zu bytes de relleno.\n", longitud, partes, tamanoParte, resto, relleno);
	} else {
		printf("El archivo de %zu bytes, se divide en %d partes.\nCada parte pesa %zu bytes.\n", longitud, partes, tamanoParte);
	}

	buffer = (char*)malloc(tamanoTrozo);		// buffer de lectura
	xorResult = (char*)malloc(tamanoTrozo);		// resultado temporal del xor
	nombreParte = (char*)malloc(FILE_MAX*sizeof(char));		// ruta de archivo

	pipesWrite = (int**)malloc((partes)*sizeof(int*));		// hacemos malloc y declaracion de los pipes
	for (j = 0; j < (partes); j++) {
		pipesWrite[j] = (int*)malloc(2*sizeof(int));
		pipe(pipesWrite[j]);
	}

	//identificarProceso(i);		

	for (i = 0; i < partes+1; i++) {		// eliminamos las partes (si existen de antes)
		if (i<partes) {
			sprintf(nombreParte, "%s.part%d", nombreArchivo, i);
			remove(nombreParte);
		}
		else {
			sprintf(nombreParte, "%s.xor", nombreArchivo);	
			remove(nombreParte);
		}
	}

	for (trozoActual=0; trozoActual<trozos; trozoActual++) {	// leemos los Trozos uno por uno

		for (i = 0; i < partes+1; i++) {	// creamos un proceso por cada parte, mas una parte xor
			pid = fork();					// i es el identificador de cada proceso, y de cada parte del archivo

		if (pid == 0) {		// procesamiento de hijos
			//identificarProceso(i);
			//printf("Trozo: %d\n", trozoActual);

			if (i<partes) {			// abrimos el archivo que corresponde a la parte i
				sprintf(nombreParte, "%s.part%d", nombreArchivo, i);
				if ((archivoDestino = fopen(nombreParte, "ab")) == NULL)  {
					printf("No se ha podido crear el archivo de destino!\n");
					return 0;
				}
			}
			else {					// si es la ultima parte, la leemos como xor
				sprintf(nombreParte, "%s.xor", nombreArchivo);	
				if ((archivoDestino = fopen(nombreParte, "ab")) == NULL)  {
					printf("No se ha podido crear el archivo de recuperación!\n");
					return 0;
				}
			}

			posicion = (i*tamanoParte)+(trozoActual*tamanoTrozo)-(i*offset);	// actualizamos la posicion del puntero que lee el archivo de origen
			fseek(archivoOrigen, posicion, SEEK_SET);
	  		fread(buffer, BYTES_RW, tamanoTrozo, archivoOrigen);	 //buffer guarda el pedazo del archivo como string

	  		aux=(char*)malloc(tamanoTrozo);			// generamos una parte auxiliar con datos básicos (nº de partes y relleno)
	  		sprintf(aux,"%s.aux",nombreArchivo);
	  		archivoAux=fopen(aux,"wb");
	  		if (i>0) {		// si el proceso no maneja la primera parte
	  			if (i==partes) {		//si es la ultima parte (no xor)
	  				fprintf(archivoAux, "%i\n", partes);			// fijamos el archivo aux
	  				if (longitud%partes!=0) {
	  					fprintf(archivoAux, "%zu\n", relleno);
	  				}
	  				else {
	  					fprintf(archivoAux, "%d\n",0);
	  				}

	  				read(pipesWrite[i-1][READ], xorResult, tamanoTrozo);		// leemos el pipe del elemento anterior con el resultado del xor
	  				fwrite(xorResult, BYTES_RW, tamanoTrozo, archivoDestino);	// escribimos el nuevo xor en el pipe

	  			}
	  			else {
	  				read(pipesWrite[i-1][READ], xorResult, tamanoTrozo);		// leemos el pipe con el xor anterior
	  			}
	  		}

	  		if (i<partes) {		// si el proceso no maneja la parte xor
	  			if (trozoActual==trozos-1) {		// si es el ultimo trozo
	  				fseek(archivoDestino, 0, SEEK_END);
	  				fwrite(buffer, BYTES_RW, tamanoTrozo-offset, archivoDestino);		// escribimos la fraccion de archivo del ultimo trozo
	  			}
	  			else {
	  				fseek(archivoDestino, 0, SEEK_END);
	  				fwrite(buffer, BYTES_RW, tamanoTrozo, archivoDestino);			// escribimos el trozo en el archivo fraccionado
	  			}

	  			if (i==0) {			// si es el proceso que maneja la primera parte
	  				xorResult = buffer;			// se copia el buffer como xor en el pipe
	  				write(pipesWrite[i][WRITE], xorResult, tamanoTrozo);
	  			}
	  			else {				// si es otro proceso (no xor)
	  				xorResult = hacerXOR(xorResult, buffer, tamanoTrozo);		// realizamos el xor entre el buffer y el xor acumulado, y lo escribimos en el pipe
	  				write(pipesWrite[i][WRITE], xorResult, tamanoTrozo);	
	  			}
	  		}
	  		fclose(archivoDestino); 	// cerramos la parte destino y cerramos el proceso
	  		exit(0);
	  	} 
	  	else {		// procesamiento del padre
	  		wait(&estado);	// esperamos a los hijos
	  	}					// notese que por cada trozo generamos un nuevo juego de procesos
	  }						// tambien notese que se divide cada parte en trozos mas pequeños (de tamaño TROZO) que puedan ser manejados por los pipes
	}

	fclose(archivoOrigen);	// cerramos el archivo de origen y terminamos la ejecucion
	printf("Procesamiento terminado!\n");
	return 0;
}